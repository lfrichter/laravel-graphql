![Laravel | center | 500x180](https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg)

## Laravel GraphQL

### Lighthouse

- [Lighthouse Documentation](https://lighthouse-php.com/)

---

### Playground
Access to test all operations

- [GraphQL Playground](http://localhost/graphql-playground).
